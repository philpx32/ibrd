import csv
import datetime
import email
import imaplib
import os
import smtplib
import socket
import ssl

import mysql.connector as MySQLdb
from mysql.connector import Error


# Read csv file from email and keep local copy
def fetch_email_attachment(email_user, email_pass, imap_host, smtp_host, imap_port, smtp_port, email_sender,
                           backup_path):
    # print(host + str(port))
    try:
        print('host ' + imap_host + ' port ' + str(imap_port))
        mail = imaplib.IMAP4_SSL(imap_host, imap_port)
        mail.login(email_user, email_pass)
        mail.select('Inbox')
        print("login success")
        type, data = mail.search(None, '(UNSEEN)', '(FROM "%s")' % email_sender)
        mail_ids = data[0]
        id_list = mail_ids.split()
        for num in data[0].split():
            typ, data = mail.fetch(num, '(RFC822)')
            raw_email = data[0][1]
            # converts byte literal to string removing b''
            raw_email_string = raw_email.decode('utf-8')
            email_message = email.message_from_string(raw_email_string)
            # downloading email attachment
            for part in email_message.walk():
                if part.get_content_maintype() == 'multipart':
                    continue
                if part.get('Content-Disposition') is None:
                    continue
                file_name = part.get_filename()
                if bool(file_name):
                    filePath = os.path.join(backup_path, file_name)
                    if not os.path.isfile(filePath):
                        fp = open(filePath, 'wb')
                        fp.write(part.get_payload(decode=True))
                        fp.close()
                    subject = str(email_message).split("Subject: ", 1)[1].split("\nTo:", 1)[0]
                    print('Downloaded "{file}" from email titled "{subject}"'.format(file=file_name, subject=subject))
                    upload_data(filePath, email_user, email_pass, smtp_host, smtp_port, email_sender, file_name)
    except imaplib.IMAP4.error as email_error:
        print("Unable to login to: " + email_user + "with pass " + email_pass)
        print(email_error)


# Load the data from the csv file into the database
def upload_data(csvFile, email_user, email_pass, smtp_host, smtp_port, email_sender, file_name):
    print("start db")
    try:
        mydb = MySQLdb.connect(host='localhost', user='root', passwd='', database='awamo_ibrd', autocommit=True)
        cursor = mydb.cursor()

        # with open('D:/_PhillipArinaitwe/_MyProjects/AWAMO/test_file.csv') as csv_file:
        with open(csvFile) as csv_file:
            start_datetime = datetime.datetime.now()
            csv_data = csv.reader(csv_file)
            insert_loan = "INSERT INTO loan(end_date, loan_number, region, country_code, borrower, " \
                          "guarantor_ccode, loan_type, loan_status, interest_rate,currency,projectid," \
                          "principal_amt,cancel_amt,undisbursed_amt,disbursed_amt,repaid_amt,due_amt," \
                          "exchange_rate,borrower_ob,sold_3rd,repaid_3rd,due_3rd,loans_held) VALUES(%s, %s, %s, " \
                          "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            insert_loan_location = "INSERT ignore INTO loan_location(country_code,  country_name) VALUES(%s, %s)"
            insert_g_location = "INSERT ignore INTO guarantor_location(country_code,  country_name) VALUES(%s, %s)"
            insert_project_location = "INSERT ignore INTO project_details(projectid,  project_name) VALUES(%s, %s)"
            insert_loan_dates = "INSERT  INTO loan_dates(end_date, loan_number,f_repay_date," \
                                "l_repay_date,agree_sign_date,board_app_date,effect_date,closed_date," \
                                "l_disburse_date) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)"
            record_cout = 0
            for row in csv_data:
                val = (row[0], row[1], row[2], row[3], row[5], row[6], row[8], row[9], row[10], row[11], row[12],
                       row[14], row[15], row[16], row[17], row[18], row[19], row[20], row[21], row[22], row[23],
                       row[24], row[25])
                loc_val = (row[3], row[4])
                g_val = (row[6], row[7])
                p_val = (row[12], row[13])
                dates_val = (row[0], row[1], row[26], row[27], row[28], row[29], row[30], row[31], row[32])
                try:
                    # print(val)
                    cursor.execute(insert_loan, val)
                    record_cout += 1
                    cursor.execute(insert_loan_location, loc_val)
                    cursor.execute(insert_g_location, g_val)
                    cursor.execute(insert_project_location, p_val)
                    cursor.execute(insert_loan_dates, dates_val)
                except Error as error:
                    print(error)
                    mydb.rollback()
            mydb.commit()
            end_datetime = datetime.datetime.now()
            print(record_cout, "record inserted.")
            print("The script started at " + str(start_datetime) + ".\n" + "The script ended at " + str(end_datetime))
            cursor.close()
        print("Done")
        send_proccess_mail(record_cout, start_datetime, email_user, email_pass, smtp_host, smtp_port, email_sender,
                           file_name)
    except Error as error:
        print(error)


# send feedback email on completion
def send_proccess_mail(records_added, date_processed, email_user, email_pass, smtp_host, smtp_port, email_sender,
                       fileName):
    try:
        message = f"""Subject: {fileName} status report

        Records Processed {records_added} \n
        Date of Processing {date_processed}
        """

        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(smtp_host, smtp_port, context=context) as server:
            server.login(email_user, email_pass)
            server.sendmail(email_user, email_sender, message)
            print("Status Mail sent to " + email_sender)

    except socket.error as smtp_error:
        print("Status Mail not sent")
        print(smtp_error)


# def upload_data(csvFile, email_user, email_pass, smtp_host, smtp_port, email_of_sender, file_name):
upload_data('D:/_PhillipArinaitwe/_MyProjects/AWAMO/ibrd-statement-of-loans-historical-data.csv', 'philpx32@gmail.com',
            'your-password', 'smtp.gmail.com', 465, 'philliparinaitwe@wingersoft.co.ug',
            'ibrd-statement-of-loans-historical-data.csv')
# def fetch_email_attachment(email_user, email_pass, email_SSL_host, imap_port, smtp_port,backup_path)
# fetch_email_attachment('philpx32@gmail.com', 'your-password', 'imap.gmail.com', 'smtp.gmail.com', 993, 465,
#                    'philliparinaitwe@wingersoft.co.ug', 'D:/_PhillipArinaitwe/_MyProjects/AWAMO')
